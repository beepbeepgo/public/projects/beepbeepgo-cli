## [1.0.3](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/compare/1.0.2...1.0.3) (2022-05-12)


### Bug Fixes

* try the CI_JOB_TOKEN ([3ecd311](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/3ecd311e4d5b7960123f26c909ba86ead0df1996))

## [1.0.2](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/compare/1.0.1...1.0.2) (2022-05-12)


### Bug Fixes

* try a specific token. ([974ea7e](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/974ea7ef8b9f26d9beb0a94bfdb6a87023d7a056))

## [1.0.1](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/compare/1.0.0...1.0.1) (2022-05-11)


### Bug Fixes

* try and release ([349f7e4](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/349f7e41d4428de711891cb3a8e85aae012dd599))

# 1.0.0 (2022-05-11)


### Bug Fixes

* add version at 0.0.0 ([328d3f7](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/328d3f798cf76d1ac6131c625e75ab114af18b09))
* fix package name in releaserc ([4cd6bc0](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/4cd6bc0e32eaa4ae49b94905ef70a3e6ecb3e904))
* fuck off. ([83a18c2](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/83a18c2fd9c805ce864cba13e27dc80f5b3b507a))
* fuck. ([0c94fe3](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/0c94fe354ec7b4d9f0220ba7e96b280bb9df2996))
* poetry publish ([fa6db2f](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/fa6db2fe082441a2bbde1ffaea96cf4e018f3063))
* python image name ([fb83fcb](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/fb83fcba9a6c1a67ef70e6f4b4135c1df434d785))
* release me ([23ba78a](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/23ba78a5dc5c3670c0cfa2c84417845159081910))
* release me! ([e42bf01](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/e42bf019afb496ed3816e02cf7829da791bbc746))
* remove docker commands from release ([600a3c7](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/600a3c73fef5b1f06b5fcf865b2e8b327349a1f7))
* remove semantic-release-python from verify ([ff4ccd0](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/ff4ccd09dbff41e1a8005a7fe1f1416b69f87285))
* removed version ([c86d0b0](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/c86d0b05aaf94f851a0ab9487e6b2cffebf938ae))
* this shall finally work. ([eb20e39](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/eb20e39e103eb8f617a08fbe8ae20a6f0cbf091a))
* try again ([72226a1](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/72226a17272347098951d7a124cd2a420925f46b))
* upgrade pip ([67b3771](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/67b37714cb4bcde584c6214d30537426d7a443c3))


### Features

* Initial Release ([1c07a83](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/1c07a830b77c073700d3fa72a4f4d136d512f0c1))
* whoooo ([265878f](https://gitlab.com/beepbeepgo/public/projects/beepbeepgo-cli/commit/265878f9b75b6a8dda9767c289a3205d6abe2159))
