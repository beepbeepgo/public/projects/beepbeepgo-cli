#!/usr/bin/env python

import importlib
import os
from .libs.cli import CLIArguments


__version__ = "0.1.1"


def load_class(class_as_path) -> object:
    """Import modules from modules dir

    Load a module from a string.  This allows for glorious modularity and
    minimal changes to the core framework.

    Args:
      full_class_string (str): Full path to the modules class.  The
        Module's directory and the class must be named the name the same with
        the exception that the directory is lowercase while the class has its
        first letter capitalized.

    Examples:
      >>> print(load_class('modules.test.view.Test')
      <class 'beepbeepgo_cli.modules.test.view.Test'>

    """
    # Parse string for class and its path
    class_data = class_as_path.split(".")
    module_path = ".".join(class_data[:-1])
    class_str = class_data[-1]

    # Import the module and instantiate the desired module
    module = importlib.import_module(f"beepbeepgo_cli.{module_path}")
    return getattr(module, class_str)


def run():
    """Execute CLI program"""
    # CLIArguments parses the global.json, and arguments.json for the
    # modules command line arguments"""
    cli = CLIArguments(os.path.dirname(os.path.realpath(__file__)))
    cli.run()

    # Based on the param called for what module to use, instantiate the class
    # and call the run() method.
    cli.args.__dict__["program_path"] = cli.program_path
    module = load_class(cli.get_module_path())
    module(**cli.args.__dict__).run()
