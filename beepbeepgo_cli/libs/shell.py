import os
import subprocess


class Shell:
    """Execute shell commands

    Args:
        path (str): Current working directory
    """

    def __init__(self):
        self.path = os.getcwd()

    def runCommand(self, command, path=""):
        """Exec a shell command via subprocess.Popen

        Args:
            command (str): String of the command to perform
            path (str: optional): Defaults to self.path
        """
        if not path:
            path = self.path
        print(f"Executing '{command}' in '{path}'")
        command = command.split()
        process = subprocess.Popen(command, cwd=path)
        process.wait()
