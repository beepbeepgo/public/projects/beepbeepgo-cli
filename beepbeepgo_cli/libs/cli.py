#!/usr/bin/env python

import argparse
import json
import os
import sys


class CLIArguments:
    """Consume the arguments.json and transform into argparse commands

    The object will take the `global.json` and `arguments.json` within each
    module.  It transforms the json into an object that's transformed into
    argparse.

    Args:
        program_path (str): Programs root path

    Attributes:
        parser (obj): Instantiated argparse.ArgumentParser()
    """

    def __init__(self, program_path):
        self.program_path = program_path
        self.parser = argparse.ArgumentParser()

    def load_json_file(self, file_path) -> list:
        """Load json file

        Args:
            file_path (str): Full directory path to arguments json file
        """
        with open(file_path) as f:
            try:
                data = json.load(f)
            except:
                data = f.readlines()
        return data

    @property
    def get_module(self):
        """Get the module name from the executed command."""
        self.command = self.args.__dict__["command"]
        return self.command

    def get_module_path(self) -> str:
        """Get the module's full path to the objects class name.

        The method is opinionated in that the module directory name and the
        objects name is the same.  However, the objects name is capitalized.

        Example:
            >>> print(get_module_path())
            'modules.example.view.Example'
        """
        return "modules.{0}.view.{1}".format(
            self.get_module, self.get_module.capitalize()
        )

    @property
    def create_sub_arguments(self):
        """Create Arguments for sub-commands.

        Each module is considered a sub-command.  In order to apply the args to
        a given module/sub-command, we initialize it as a sub parser.  This
        allows the object to list the arguments under the correct modules.
        """
        self.subparser = self.parser.add_subparsers(
            help="Active Modules", dest="command"
        )
        for module in self.load_modules:
            help_info = "{0} module".format(module.capitalize())
            self.module = self.subparser.add_parser(module, help=help_info)
            self.append_sub_arguments("global")
            self.append_sub_arguments(module)

    def append_sub_arguments(self, module):
        """Read and append arguments to sub-commands.

        Args:
            module (str): Name of a module to append command arguments to.
        """
        try:
            if module == "global":
                file_path = "{0}/{1}.json".format(self.program_path, module)
            else:
                file_path = "{0}/modules/{1}/arguments.json".format(
                    self.program_path, module
                )
            for item in self.load_json_file(file_path):
                self.module.add_argument(item.pop("long"), item.pop("short"), **item)
        except Exception as err:
            # This is dum
            print(err)
            sys.exit(1)

    @property
    def parse_arguments(self):
        """Parse arguments on command line execution"""
        # create arguments
        self.create_sub_arguments
        self.args = self.parser.parse_args()

    @property
    def load_modules(self):
        """Load Modules into the root level of the argparse command utility

        This instantiates each module in the root level of the command line.
        Any module located in './modules' has its arguments loaded and waiting
        for execution.
        """
        modules = []
        path = "{0}/modules".format(self.program_path)
        for item in os.listdir(path):
            # change this to regex matching
            if ".py" not in item and "__pycache__" not in item:
                modules.append(item)
        return modules

    def run(self):
        self.parse_arguments
