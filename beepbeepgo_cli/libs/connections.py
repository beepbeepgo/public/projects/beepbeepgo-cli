import boto3
from botocore.exceptions import ClientError


class AWSConnections:
    def __init__(self, aws_account, role_name):
        self.aws_account = aws_account
        self.role_name = role_name
        self.temp_creds = {}

    def role_arn(self, aws_account="", role_name=""):
        if not aws_account:
            aws_account = self.aws_account
        if not role_name:
            role_name = self.role_name
        arn = "arn:aws:iam::{0}:role/{1}".format(aws_account, role_name)
        return arn

    def assume_account(
        self,
        role_arn="",
        role_session_name="beepbeepgo-cli",
        session_duration_seconds=900,
    ):
        if not role_arn:
            role_arn = self.role_arn(self.aws_account, role_session_name)

        try:
            client = boto3.client("sts")
            self.temp_creds = client.assume_role(
                RoleArn=self.role_arn(),
                RoleSessionName=role_session_name,
                DurationSeconds=session_duration_seconds,
            )["Credentials"]
        except ClientError as err:
            print(f"Invalid credentials: {err}")
            exit(1)
        return self.temp_creds

    def connect_to_client_service(self, service, region):
        client = boto3.client(
            service,
            aws_access_key_id=self.temp_creds["AccessKeyId"],
            aws_secret_access_key=self.temp_creds["SecretAccessKey"],
            aws_session_token=self.temp_creds["SessionToken"],
            region_name=region,
        )
        return client

    def connect_to_resource_service(self, service, region):
        resource = boto3.resource(
            service,
            aws_access_key_id=self.temp_creds["AccessKeyId"],
            aws_secret_access_key=self.temp_creds["SecretAccessKey"],
            aws_session_token=self.temp_creds["SessionToken"],
            region_name=region,
        )
        return resource
