#!/usr/bin/env python


class Test:
    def __init__(self, **kwargs):
        self.args = kwargs
        self.hello_world = self.args["hello_world"]

    def run(self):
        print(f"hello {self.hello_world}")
